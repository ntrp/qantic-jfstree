/*
	This file is part of qantic-jfstree.

    qantic-jfstree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qantic-jfstree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qantic-jfstree.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.qantic.ui.swing.jfstree;

import java.io.File;

import javax.swing.table.TableModel;

public interface IFsTreeTableModel extends TableModel{

	String[] getExtensionList();

	void setFileList(File[] fileList);

}
