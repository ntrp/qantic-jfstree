/*
	This file is part of qantic-jfstree.

    qantic-jfstree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qantic-jfstree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qantic-jfstree.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.qantic.ui.swing.jfstree;

import java.awt.Color;
import java.io.File;

import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class JFsTree extends JTree {

	private static final long serialVersionUID = -7846342150204228156L;
	// Using filechooser api to access file system
	private final FileSystemView fsView = FileSystemView.getFileSystemView();
	FsTreeSelectionListener selectionListener = null;
	FsTreeWillExpandListener expandListner = null;

	public JFsTree() {

		this(null);
	}

	public JFsTree(JTable detailTable) {

		super();
		// Creating tree structure
		DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode();
		DefaultTreeModel fsTreeModel = new DefaultTreeModel(rootNode);
		// Generating starting structure, roots and children
		for (File fsRoot : fsView.getRoots()) {
			DefaultMutableTreeNode fsRootNode = new DefaultMutableTreeNode(fsRoot);
			rootNode.add(fsRootNode);
			for (File fsChild : fsView.getFiles(fsRoot, true)) {
				if (fsChild.isDirectory()) {
					fsRootNode.add(new DefaultMutableTreeNode(fsChild));
				}
			}
		}
		// Set tree model to the generated one
		this.setModel(fsTreeModel);
		// Removing the aggregating root node
		this.setRootVisible(false);
		// Setting background to white in case of special LAF
		this.setBackground(Color.WHITE);
		// Creating and setting listeners
		selectionListener = new FsTreeSelectionListener(detailTable);
		expandListner = new FsTreeWillExpandListener((DefaultTreeModel) this.getModel());
		this.addTreeWillExpandListener(expandListner);
		this.addTreeSelectionListener(selectionListener);
		this.setCellRenderer(new FsTreeCellRenderer());
		// Expanding the first row
		this.expandRow(0);
	}

	public void setDetailTable(JTable table) {

		selectionListener.setDetailTable(table);
	}

}
