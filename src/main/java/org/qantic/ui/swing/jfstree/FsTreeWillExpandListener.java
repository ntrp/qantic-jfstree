/*
	This file is part of qantic-jfstree.

    qantic-jfstree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qantic-jfstree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qantic-jfstree.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.qantic.ui.swing.jfstree;

import java.io.File;

import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;

class FsTreeWillExpandListener implements TreeWillExpandListener {

	private final FileSystemView fsView = FileSystemView.getFileSystemView();
	private final DefaultTreeModel fsTreeModel;

	public FsTreeWillExpandListener(DefaultTreeModel treeModel) {

		fsTreeModel = treeModel;
	}

	@Override
	public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {

	}

	@Override
	public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException {

		final DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) event.getPath().getLastPathComponent();
		// On expand load two levels of children
		this.lazyLoadChildren(selectedNode, 2);
	}

	private boolean isChildPresent(DefaultMutableTreeNode node, File checkFile) {

		// Check if the file provided is a child of the current node
		for (int i = 0; i < node.getChildCount(); i++) {
			if (((DefaultMutableTreeNode) node.getChildAt(i)).getUserObject().equals(checkFile)) {
				return true;
			}
		}
		return false;
	}

	private void lazyLoadChildren(DefaultMutableTreeNode currentNode, int depth) {

		final File currentDir = (File) currentNode.getUserObject();
		final File[] currentDirChildren = fsView.getFiles(currentDir, true);

		// Check for deleted nodes
		for (int i = 0; i < currentNode.getChildCount(); i++) {
			File chidlFile = (File) ((DefaultMutableTreeNode) currentNode.getChildAt(i)).getUserObject();
			if (!chidlFile.exists()) {
				// If the child is missing on the file system remove it from the tree
				currentNode.remove(i);
			}
		}
		// Check for new nodes
		for (File child : currentDirChildren) {
			if (child.isDirectory() && !this.isChildPresent(currentNode, child)) {
				// Found new child directory
				currentNode.add(new DefaultMutableTreeNode(child));
			}
		}
		// If depth > 1 recurse to load sub children
		if (depth > 1) {
			for (int i = 0; i < currentNode.getChildCount(); i++) {
				this.lazyLoadChildren(((DefaultMutableTreeNode) currentNode.getChildAt(i)), depth - 1);
			}
		}
		// notify the TreeModel that the tree has changed
		fsTreeModel.nodeStructureChanged(currentNode);
	}
}