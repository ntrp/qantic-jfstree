/*
	This file is part of qantic-jfstree.

    qantic-jfstree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qantic-jfstree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qantic-jfstree.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.qantic.ui.swing.jfstree;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

class FsTreeCellRenderer extends DefaultTreeCellRenderer {

	private static final long serialVersionUID = 7032942392760178392L;
	private final FileSystemView sfView = FileSystemView.getFileSystemView();
	private final JFileChooser jfc;

	public FsTreeCellRenderer() {
		jfc = new JFileChooser();
	}

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {

		JLabel c = (JLabel) super.getTreeCellRendererComponent(tree, value, isSelected, expanded, leaf, row, hasFocus);
		if (isSelected) {
			c.setOpaque(false);
			c.setForeground(this.getTextSelectionColor());
		} else {
			c.setOpaque(true);
			c.setForeground(this.getTextNonSelectionColor());
			c.setBackground(this.getBackgroundNonSelectionColor());
		}
		if (value instanceof DefaultMutableTreeNode) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
			Object o = node.getUserObject();
			if (o instanceof File) {
				File file = (File) o;
				c.setIcon(jfc.getIcon(file));
				c.setText(this.sfView.getSystemDisplayName(file));
				c.setToolTipText(file.getPath());
			}
		}
		return c;
	}
}