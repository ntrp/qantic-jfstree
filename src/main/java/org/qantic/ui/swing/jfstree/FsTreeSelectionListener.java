/*
	This file is part of qantic-jfstree.

    qantic-jfstree is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    qantic-jfstree is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with qantic-jfstree.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.qantic.ui.swing.jfstree;

import java.io.File;

import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

class FsTreeSelectionListener implements TreeSelectionListener {

	private JTable viewTable;

	public FsTreeSelectionListener(JTable vtab) {

		viewTable = vtab;
	}

	@Override
	public void valueChanged(TreeSelectionEvent e) {

		if (viewTable != null) {

			final DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
			final File selectedDir = (File) selectedNode.getUserObject();

			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {

					final IFsTreeTableModel musicTableModel = (IFsTreeTableModel) viewTable.getModel();
					File[] fileList = selectedDir.listFiles(new CustomFileFilter(musicTableModel.getExtensionList(), true));
					musicTableModel.setFileList(fileList);
				}
			});
		}
	}

	public void setDetailTable(JTable vtab) {

		viewTable = vtab;
	}
}